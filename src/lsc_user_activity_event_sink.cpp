/*
 * Copyright © 2016 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lsc_user_activity_event_sink.h"
#include "lsc_user_activity_type.h"
#include "dbus_message_handle.h"

namespace
{
char const* const lsc_user_activity_name = "com.lomiri.SystemCompositor.UserActivity";
char const* const lsc_user_activity_path = "/com/lomiri/SystemCompositor/UserActivity";
char const* const lsc_user_activity_iface = "com.lomiri.SystemCompositor.UserActivity";
}

lsc::LscUserActivityEventSink::LscUserActivityEventSink(
    std::string const& dbus_address)
    : dbus_connection{dbus_address}
{
    dbus_connection.request_name(lsc_user_activity_name);
}

void lsc::LscUserActivityEventSink::notify_activity_changing_power_state()
{
    int const changing_power_state =
        static_cast<int>(LscUserActivityType::changing_power_state);

    DBusMessageHandle signal{
        dbus_message_new_signal(
            lsc_user_activity_path,
            lsc_user_activity_iface,
            "Activity"),
        DBUS_TYPE_INT32, &changing_power_state,
        DBUS_TYPE_INVALID};

    dbus_connection_send(dbus_connection, signal, nullptr);
    dbus_connection_flush(dbus_connection);
}

void lsc::LscUserActivityEventSink::notify_activity_extending_power_state()
{
    int const extending_power_state =
        static_cast<int>(LscUserActivityType::extending_power_state);

    DBusMessageHandle signal{
        dbus_message_new_signal(
            lsc_user_activity_path,
            lsc_user_activity_iface,
            "Activity"),
        DBUS_TYPE_INT32, &extending_power_state,
        DBUS_TYPE_INVALID};

    dbus_connection_send(dbus_connection, signal, nullptr);
    dbus_connection_flush(dbus_connection);
}
